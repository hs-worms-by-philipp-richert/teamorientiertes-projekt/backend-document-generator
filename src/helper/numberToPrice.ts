export const numberToPrice = (price: number) => {
  const fixedPrice = price.toFixed(2);
  
  const priceString = fixedPrice.toString()+'€';
 
  return priceString.replace('.', ',');
}