export const dateToEuropeanFormat = (date: Date): string => {
  const day =
    date.getDate() > 9
      ? date.getDate().toString()
      : '0' + date.getDate().toString();

  const month =
    date.getMonth() > 9
      ? date.getMonth().toString()
      : '0' + (date.getMonth() + 1).toString();

  return `${day}.${month}.${date.getFullYear()}`;
};
