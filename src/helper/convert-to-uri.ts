import fs from 'fs/promises';
import mime from 'mime';

export const convertToUri = async (imagePath: string) => {
  try {
    const content = await fs.readFile(imagePath);
    const b64 = content.toString('base64');
    const type = mime.lookup(imagePath);
    return `data:${type};base64,${b64}`;
  
  } catch (error) {
    console.error("Error converting image to Data URI:", error);
    return null;
  }
};
