import { CRUD } from '../shared/interfaces/crud';
import nodemailer from 'nodemailer';
import dotenv from 'dotenv';
import ejs from 'ejs';
import { ILineItem } from '../shared/interfaces/ILineItem';
import { dateToEuropeanFormat } from '../helper/dateToEuropeanFormat';
dotenv.config();

export class EMailService implements CRUD {
  private transporter;
  private eMail;
  private nameMailOwner;

  constructor() {
    this.transporter = nodemailer.createTransport({
      host: process.env.SMTPHOST,
      port: parseInt(process.env.SMTPPORT!.toString()) || 25,
      secure: false,
      auth: {
        user: process.env.EMAILUSER,
        pass: process.env.EMAILPASSWORD,
      },
    });

    this.eMail = 'info@deutsche-alpen-verein.de';
    this.nameMailOwner = 'Info DAV';
  }

  list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }
  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  async create(resource: any): Promise<any> {
    const { receiver, subject, htmlFileName, pdfBuffer, options } = resource;

    const htmlBody: string = await ejs.renderFile(
      `./src/views/emails/${htmlFileName}.ejs`,
      options
    );
    if (pdfBuffer) {
      await this.transporter.sendMail({
        from: `${this.nameMailOwner} <${this.eMail}>`,
        to: receiver,
        subject: subject,
        html: htmlBody,
        attachments: [
          {
            filename: `order-${options.order.OrderId}.pdf`,
            content: pdfBuffer,
            contentType: 'application/pdf',
            contentDisposition: 'attachment',
          },
        ],
      });
      return;
    }

    await this.transporter.sendMail({
      from: `${this.nameMailOwner} <${this.eMail}>`,
      to: receiver,
      subject: subject,
      html: htmlBody,
    });
  }
  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  readById(id: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  deleteById(id: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}
