import puppeteer from 'puppeteer';
import { CRUD } from '../shared/interfaces/crud';
import ejs from 'ejs';
import { convertToUri } from '../helper/convert-to-uri';
import { dateToEuropeanFormat } from '../helper/dateToEuropeanFormat';
import { numberToPrice } from '../helper/numberToPrice';
import { IOrder } from '../shared/interfaces/IOrder';
import { ILineItem } from '../shared/interfaces/ILineItem';

export class PDFService implements CRUD {
  list(take: number, page: number): Promise<any> {
    throw new Error('Method not implemented.');
  }
  listFiltered(take: number, page: number, filterOptions: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  async create(orderData: IOrder): Promise<any> {
    const logoPath = 'public/assets/img/user-content/main-logo.svg';
    const logoDataUri = await convertToUri(logoPath);

    if (!logoDataUri) throw 'Error';
    const footerHtml = await ejs.renderFile('src/views/partials/footer.ejs');
    const headerHtml = await ejs.renderFile('src/views/partials/header.ejs', {
      logoImageUri: logoDataUri,
    });

    const userData = orderData.User;
    const lineItemsData = orderData.LineItems;

    const beginSorted = lineItemsData.sort((a: ILineItem, b: ILineItem) => {
      return new Date(a.StartDate).getTime() - new Date(b.StartDate).getTime();
    });

    const endSorted = lineItemsData.sort((a: ILineItem, b: ILineItem) => {
      return new Date(a.EndDate).getTime() - new Date(b.EndDate).getTime();
    });

    if (!orderData || orderData === undefined || !orderData.LineItems) return;
    
    const mainHtml = await ejs.renderFile(
      'src/views/partials/order-invoice.ejs',
      {
        order: orderData,
        orderBegin: dateToEuropeanFormat(new Date(beginSorted[0].StartDate)),
        orderEnd: dateToEuropeanFormat(
          new Date(endSorted[lineItemsData.length - 1].EndDate)
        ),
        dateToEuropeanFormatFunction: dateToEuropeanFormat,
        numToPrice: numberToPrice,
        user: userData,
      }
    );

    const htmlContent = await ejs.renderFile(
      'src/views/pdfs/order-invoice-pdf.ejs',
      {
        header: headerHtml,
        footer: footerHtml,
        main: mainHtml,
      }
    );

    const browser = await puppeteer.launch({
      args: ['--allow-file-access-from-files'],
    });
    const page = await browser.newPage();

    await page.setContent(htmlContent, {
      waitUntil: 'domcontentloaded',
    });

    const pdf = await page.pdf({
      format: 'A4',
      printBackground: true,
    });

    await browser.close();

    return pdf;
  }

  putById(id: any, resource: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  readById(id: any): Promise<any> {
    throw new Error('Method not implemented.');
  }
  deleteById(id: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  patchById(id: any, resources: any): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}
