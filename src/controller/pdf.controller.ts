import { Request, Response } from 'express';
import { PDFService } from '../services/pdf.service';
import { IOrder } from '../shared/interfaces/IOrder';
import { HttpCode } from '../shared/enums/http-codes.enum';
import errorHandler from '../helper/error-handler';

const pdfService = new PDFService();

export const createOrderInvoicePDF = async (req: Request, res: Response) => {
  try {
    const order = req.body as IOrder;

    const pdfBuffer = await pdfService.create(order);

    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', 'attachment; filename=invoice.pdf');
    res.status(HttpCode.OK).send(pdfBuffer);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
