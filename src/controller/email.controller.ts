import { Request, Response } from "express";
import { EMailService } from "../services/email.service";
import { HttpCode } from "../shared/enums/http-codes.enum";
import { PDFService } from "../services/pdf.service";
import { IOrder } from "../shared/interfaces/IOrder";
import errorHandler from "../helper/error-handler";
import { ErrorType } from "../shared/enums/error-types.enum";
import { ILineItem } from "../shared/interfaces/ILineItem";
import { dateToEuropeanFormat } from "../helper/dateToEuropeanFormat";
import { IUser } from "../shared/interfaces/IUser";

const eMailService = new EMailService();
const pdfService = new PDFService();

export const createOrderInvoiceEMailCustomer = async (
  req: Request,
  res: Response
) => {
  try {
    const { receiver } = req.body;
    const order = req.body.order as IOrder;

    if (!receiver || !order || !order.User)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const htmlFileName = "order-invoice-customer";
    const pdfBuffer = await pdfService.create(order);
    const subject =
      "Bestellbestätigung für Ihre Ausleihe / Order confirmation for your rental";

    const lineItemsData = order.LineItems;

    const beginSorted = lineItemsData.sort((a: ILineItem, b: ILineItem) => {
      return new Date(a.StartDate).getTime() - new Date(b.StartDate).getTime();
    });

    const endSorted = lineItemsData.sort((a: ILineItem, b: ILineItem) => {
      return new Date(a.EndDate).getTime() - new Date(b.EndDate).getTime();
    });

    const options = {
      order: order,
      begin: dateToEuropeanFormat(new Date(beginSorted[0].StartDate)),
      end: dateToEuropeanFormat(
        new Date(endSorted[endSorted.length - 1].EndDate)
      ),
    };

    const data = {
      receiver: receiver,
      subject: subject,
      htmlFileName: htmlFileName,
      pdfBuffer: pdfBuffer,
      options: options,
    };

    await eMailService.create(data);

    const optionsEmployee = {
      order: order,
      begin: dateToEuropeanFormat(new Date(beginSorted[0].StartDate)),
      end: dateToEuropeanFormat(
        new Date(endSorted[endSorted.length - 1].EndDate)
      ),
    };

    const htmlFileNameEmployee = "order-invoice-employee";
    const subjectEmployee = `Neue Bestellung zum ${dateToEuropeanFormat(new Date(beginSorted[0].StartDate))}`;

    const dataEmployee = {
      receiver: "dav@info.de",
      subject: subjectEmployee,
      htmlFileName: htmlFileNameEmployee,
      orderData: order,
      pdfBuffer: pdfBuffer,
      options: optionsEmployee,
    };

    await eMailService.create(dataEmployee);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const orderCancelation = async (req: Request, res: Response) => {
  try {
    const { receiver } = req.body;
    const order = req.body.order as IOrder;
    const subject =
      "Ihre Bestellung wurde storniert / Your order has been canceled";

    if (!receiver || !order || !order.User)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const htmlFileName = "order-cancelation";

    const options = {
      order: order,
    };

    const data = {
      receiver: receiver,
      subject: subject,
      htmlFileName: htmlFileName,
      options: options,
    };
    await eMailService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const readyForPickup = async (req: Request, res: Response) => {
  try {
    const { receiver } = req.body;
    const order = req.body.order as IOrder;
    const subject =
      "Ihre Bestellung ist abholbereit! / Your order is ready for pickup!";

    if (!order || !order.User)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const htmlFileName = "order-ready-for-pickup";
    const pdfBuffer = await pdfService.create(order);

    const lineItemsData = order.LineItems;

    const beginSorted = lineItemsData.sort((a: ILineItem, b: ILineItem) => {
      return new Date(a.StartDate).getTime() - new Date(b.StartDate).getTime();
    });

    const options = {
      order: order,
      begin: dateToEuropeanFormat(new Date(beginSorted[0].StartDate)),
    };

    const data = {
      receiver: receiver,
      subject: subject,
      htmlFileName: htmlFileName,
      pdfBuffer: pdfBuffer,
      options: options,
    };

    await eMailService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const userRoleHasBeenUpdated = async (req: Request, res: Response) => {
  try {
    const { receiver } = req.body;
    const user = req.body.user as IUser;

    if (!receiver || !user)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );
    const subject =
      "Ihr Mitgliedsstatus wurde angepasst! / Your account has been successfully upgraded!";

    if (!user) {
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );
    }
    const htmlFileName = "user-has-been-updated";

    const options = {
      user,
    };
    const data = {
      receiver: receiver.Email,
      subject,
      htmlFileName,
      options,
    };

    await eMailService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const orderEndsInTwoDays = async (req: Request, res: Response) => {
  try {
    const { receiver } = req.body;
    const order = req.body.order as IOrder;
    const subject =
      "Erinnerung: Ihr Leihzeitraum endet in 2 Tagen / Reminder: Your rental period ends in 2 days";

    if (!receiver || !order || !order.User)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const htmlFileName = "order-ends-in-two-days";
    const lineItemsData = order.LineItems;

    const endSorted = lineItemsData.sort((a: ILineItem, b: ILineItem) => {
      return new Date(a.EndDate).getTime() - new Date(b.EndDate).getTime();
    });

    const options = {
      order: order,
      end: dateToEuropeanFormat(
        new Date(endSorted[endSorted.length - 1].EndDate)
      ),
    };

    const data = {
      receiver: receiver,
      subject: subject,
      htmlFileName: htmlFileName,
      options: options,
    };

    await eMailService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const orderEndsToday = async (req: Request, res: Response) => {
  try {
    const { receiver } = req.body;
    const order = req.body.order as IOrder;
    const subject =
      "Letzte Erinnerung: Ihr Leihzeitraum endet heute / Final Reminder: Your rental period ends today";

    if (!receiver || !order || !order.User)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const htmlFileName = "order-ends-today";

    const lineItemsData = order.LineItems;

    const endSorted = lineItemsData.sort((a: ILineItem, b: ILineItem) => {
      return new Date(a.EndDate).getTime() - new Date(b.EndDate).getTime();
    });

    const options = {
      order: order,
      end: dateToEuropeanFormat(
        new Date(endSorted[endSorted.length - 1].EndDate)
      ),
    };

    const data = {
      receiver: receiver,
      subject: subject,
      htmlFileName: htmlFileName,
      options: options,
    };

    await eMailService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const orderEndsTommorow = async (req: Request, res: Response) => {
  try {
    const { receiver } = req.body;
    const order = req.body.order as IOrder;

    const lineItemsData = order.LineItems;

    if (!order || !order.User)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const endSorted = lineItemsData.sort((a: ILineItem, b: ILineItem) => {
      return new Date(a.EndDate).getTime() - new Date(b.EndDate).getTime();
    });

    const options = {
      order: order,
      end: dateToEuropeanFormat(
        new Date(endSorted[endSorted.length - 1].EndDate)
      ),
    };

    const subject = `Erinnerung: Ihr Leihzeitraum endet morgen / Reminder: Your rental period ends tommorow`;

    const htmlFileName = "order-ends-tommorow";

    const data = {
      receiver: receiver,
      subject: subject,
      htmlFileName: htmlFileName,
      options: options,
    };

    await eMailService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const orderExceeded = async (req: Request, res: Response) => {
  try {
    const { receiver } = req.body;
    const order = req.body.order as IOrder;

    if (!receiver || !order || !order.User)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const lineItemsData = order.LineItems;

    const beginSorted = lineItemsData.sort((a: ILineItem, b: ILineItem) => {
      return new Date(a.StartDate).getTime() - new Date(b.StartDate).getTime();
    });

    const endSorted = lineItemsData.sort((a: ILineItem, b: ILineItem) => {
      return new Date(a.EndDate).getTime() - new Date(b.EndDate).getTime();
    });

    const options = {
      order: order,
      end: dateToEuropeanFormat(
        new Date(endSorted[endSorted.length - 1].EndDate)
      ),
    };

    const subject = `Erinnerung: Überzogene Rückgabe Ihrer Ausleihe / Reminder: Overdue Return of Your Rental`;
    const htmlFileName = "order-exceeded";

    const data = {
      receiver: receiver,
      subject: subject,
      htmlFileName: htmlFileName,
      options: options,
    };

    await eMailService.create(data);

    const optionsEmployee = {
      order: order,
      begin: dateToEuropeanFormat(new Date(beginSorted[0].StartDate)),
      end: dateToEuropeanFormat(
        new Date(endSorted[endSorted.length - 1].EndDate)
      ),
    };

    const subjectEmployee = `Überzogene Rückgabe bei der BestellungNr. ${order.OrderId}`;
    const htmlFileNameEmployee = "order-exceeded-employee";

    const dataEmployee = {
      receiver: "dav@info.de",
      subject: subjectEmployee,
      htmlFileName: htmlFileNameEmployee,
      options: optionsEmployee,
    };

    await eMailService.create(dataEmployee);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const welcomeNewUser = async (req: Request, res: Response) => {
  try {
    const { receiver } = req.body;
    const user = req.body.user as IUser;

    if (!receiver || !user)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const subject = `Willkommen beim Materialverleih des DAV Worms! / Welcome to DAV Worms Equipment Rental!`;

    const htmlFileName = "welcome-new-user";

    const options = {
      user: user,
    };

    const data = {
      receiver: receiver,
      subject: subject,
      htmlFileName: htmlFileName,
      options: options,
    };

    await eMailService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const psaExpiresNextMonth = async (req: Request, res: Response) => {
  try {
    const psaItems = req.body;

    if (!psaItems || !psaItems.psaAlreadyExpired || !psaItems.psaExpiringSoon)
      return errorHandler.handleControllerCancellation(
        res,
        HttpCode.BadRequest,
        ErrorType.InvalidInput
      );

    const subject = `PSA Report`;

    const htmlFileName = "psa-over-eleven-months";

    const options = {
      expiredItems: psaItems.psaAlreadyExpired,
      expiringSoonItems: psaItems.psaExpiringSoon,
    };

    const data = {
      receiver: process.env.EMAILUSER,
      subject: subject,
      htmlFileName: htmlFileName,
      options: options,
    };

    await eMailService.create(data);

    res.sendStatus(HttpCode.Created);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};

export const ordersOfLast24Hours = async (req: Request, res: Response) => {
  try {
    const orders = req.body.order as IOrder[];

    const subject = `Neue Bestellungen sind eingetroffen`;

    const htmlFileName = "new-orders";

    const options = {
      order: orders,
      dateToEuropeanFormat: dateToEuropeanFormat,
    };

    const data = {
      receiver: process.env.EMAILUSER,
      subject: subject,
      htmlFileName: htmlFileName,
      options: options,
    };

    await eMailService.create(data);
  } catch (error) {
    const errorMsg: string = errorHandler.handleControllerException(error);
    res.status(HttpCode.InternalServerError).json({ error: errorMsg });
  }
};
