import express, { Application } from 'express';
import dotenv from 'dotenv';
import { indexRouter } from './routes/index.router';

dotenv.config();

const app: Application = express();
const port = process.env.PORT || 3001;

app.use(express.json());
app.use(express.static('public'));
app.use('/',indexRouter);

app.listen(port,() => {
  console.log(`Document Generator Server is running at http://localhost:${port}`);
})