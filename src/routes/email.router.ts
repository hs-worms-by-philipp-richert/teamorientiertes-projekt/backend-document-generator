import { Router } from "express";
import {
  createOrderInvoiceEMailCustomer,
  orderCancelation,
  orderEndsInTwoDays,
  orderEndsToday,
  orderEndsTommorow,
  orderExceeded,
  ordersOfLast24Hours,
  psaExpiresNextMonth,
  readyForPickup,
  userRoleHasBeenUpdated,
  welcomeNewUser,
} from "../controller/email.controller";

const eMailRouter = Router();

eMailRouter.post("/order-invoice", createOrderInvoiceEMailCustomer);
eMailRouter.post("/ready-for-pickup", readyForPickup);
eMailRouter.post("/order-ends-in-two-days", orderEndsInTwoDays);
eMailRouter.post("/order-ends-today", orderEndsToday);

eMailRouter.post("/order-ends-tommorow", orderEndsTommorow);
eMailRouter.post("/order-exceeded", orderExceeded);
eMailRouter.post("/order-cancelation", orderCancelation);
eMailRouter.post("/welcome-new-user", welcomeNewUser);
eMailRouter.post("/user-has-been-updated", userRoleHasBeenUpdated);
eMailRouter.post("/psa-overview", psaExpiresNextMonth);

eMailRouter.post("/orders-of-last-24-hours", ordersOfLast24Hours);

export { eMailRouter };
