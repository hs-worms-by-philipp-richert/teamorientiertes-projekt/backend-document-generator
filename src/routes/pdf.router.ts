import { Router } from 'express';
import { createOrderInvoicePDF } from '../controller/pdf.controller';

const pdfRouter = Router();

pdfRouter.post('/generate/order-invoice', createOrderInvoicePDF);

export { pdfRouter };
