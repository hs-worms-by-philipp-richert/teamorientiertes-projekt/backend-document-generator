import { Request, Response, Router } from "express";
import { pdfRouter } from "./pdf.router";
import { eMailRouter } from "./email.router";

const indexRouter = Router();

indexRouter.use("/pdf", pdfRouter);
indexRouter.use("/email", eMailRouter);
indexRouter.get("/", (req: Request, res: Response) => {
  res.status(200).json({ msg: "works" });
});

export { indexRouter };
